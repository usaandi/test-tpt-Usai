var assert = require('assert');

function sum(a, b, c = 0){

    return parseInt(a, 10) + parseInt(b, 10) + parseInt(c, 10);
}
assert.sum(('1','2'), 3, '"1"+"2"=3');

assert.sum((1,2), 3, '1+2+3');

assert.sum((1,2,3),6);
